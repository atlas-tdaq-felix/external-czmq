'\" t
.\"     Title: zproxy
.\"    Author: [see the "AUTHORS" section]
.\" Generator: DocBook XSL Stylesheets vsnapshot <http://docbook.sf.net/>
.\"      Date: 02/28/2023
.\"    Manual: CZMQ Manual
.\"    Source: CZMQ 3.0.2
.\"  Language: English
.\"
.TH "ZPROXY" "3" "02/28/2023" "CZMQ 3\&.0\&.2" "CZMQ Manual"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
zproxy \- run a steerable proxy in the background
.SH "SYNOPSIS"
.sp
.nf
//  Create new zproxy actor instance\&. The proxy switches messages between
//  a frontend socket and a backend socket; use the FRONTEND and BACKEND
//  commands to configure these:
//
//      zactor_t *proxy = zactor_new (zproxy, NULL);
//
//  Destroy zproxy instance\&. This destroys the two sockets and stops any
//  message flow between them:
//
//      zactor_destroy (&proxy);
//
//  Note that all zproxy commands are synchronous, so your application always
//  waits for a signal from the actor after each command\&.
//
//  Enable verbose logging of commands and activity:
//
//      zstr_send (proxy, "VERBOSE");
//      zsock_wait (proxy);
//
//  Specify frontend socket type \-\- see zsock_type_str () \-\- and attach to
//  endpoints, see zsock_attach ()\&. Note that a proxy socket is always
//  serverish:
//
//      zstr_sendx (proxy, "FRONTEND", "XSUB", endpoints, NULL);
//      zsock_wait (proxy);
//
//  Specify backend socket type \-\- see zsock_type_str () \-\- and attach to
//  endpoints, see zsock_attach ()\&. Note that a proxy socket is always
//  serverish:
//
//      zstr_sendx (proxy, "BACKEND", "XPUB", endpoints, NULL);
//      zsock_wait (proxy);
//
//  Capture all proxied messages; these are delivered to the application
//  via an inproc PULL socket that you have already bound to the specified
//  endpoint:
//
//      zstr_sendx (proxy, "CAPTURE", endpoint, NULL);
//      zsock_wait (proxy);
//
//  Pause the proxy\&. A paused proxy will cease processing messages, causing
//  them to be queued up and potentially hit the high\-water mark on the
//  frontend or backend socket, causing messages to be dropped, or writing
//  applications to block:
//
//      zstr_sendx (proxy, "PAUSE", NULL);
//      zsock_wait (proxy);
//
//  Resume the proxy\&. Note that the proxy starts automatically as soon as it
//  has a properly attached frontend and backend socket:
//
//      zstr_sendx (proxy, "RESUME", NULL);
//      zsock_wait (proxy);
//
//  This is the zproxy constructor as a zactor_fn; the argument is a
//  character string specifying frontend and backend socket types as two
//  uppercase strings separated by a hyphen:
CZMQ_EXPORT void
    zproxy (zsock_t *pipe, void *unused);

//  Selftest
CZMQ_EXPORT void
    zproxy_test (bool verbose);
.fi
.SH "DESCRIPTION"
.sp
A zproxy actor switches messages between a frontend and a backend socket\&. It acts much like the zmq_proxy_steerable method, though it makes benefit of CZMQ\(cqs facilities, to be somewhat simpler to set\-up\&.
.sp
This class replaces zproxy_v2, and is meant for applications that use the CZMQ v3 API (meaning, zsock)\&.
.SH "EXAMPLE"
.PP
\fBFrom zproxy_test method\fR. 
.sp
.if n \{\
.RS 4
.\}
.nf
//  Create and configure our proxy
zactor_t *proxy = zactor_new (zproxy, NULL);
assert (proxy);
if (verbose) {
    zstr_sendx (proxy, "VERBOSE", NULL);
    zsock_wait (proxy);
}
zstr_sendx (proxy, "FRONTEND", "PULL", "inproc://frontend", NULL);
zsock_wait (proxy);
zstr_sendx (proxy, "BACKEND", "PUSH", "inproc://backend", NULL);
zsock_wait (proxy);

//  Connect application sockets to proxy
zsock_t *faucet = zsock_new_push (">inproc://frontend");
assert (faucet);
zsock_t *sink = zsock_new_pull (">inproc://backend");
assert (sink);

//  Send some messages and check they arrived
char *hello, *world;
zstr_sendx (faucet, "Hello", "World", NULL);
zstr_recvx (sink, &hello, &world, NULL);
assert (streq (hello, "Hello"));
assert (streq (world, "World"));
zstr_free (&hello);
zstr_free (&world);

//  Test pause/resume functionality
zstr_sendx (proxy, "PAUSE", NULL);
zsock_wait (proxy);
zstr_sendx (faucet, "Hello", "World", NULL);
zsock_set_rcvtimeo (sink, 100);
zstr_recvx (sink, &hello, &world, NULL);
assert (!hello && !world);

zstr_sendx (proxy, "RESUME", NULL);
zsock_wait (proxy);
zstr_recvx (sink, &hello, &world, NULL);
assert (streq (hello, "Hello"));
assert (streq (world, "World"));
zstr_free (&hello);
zstr_free (&world);

//  Test capture functionality
zsock_t *capture = zsock_new_pull ("inproc://capture");
assert (capture);

//  Switch on capturing, check that it works
zstr_sendx (proxy, "CAPTURE", "inproc://capture", NULL);
zsock_wait (proxy);
zstr_sendx (faucet, "Hello", "World", NULL);
zstr_recvx (sink, &hello, &world, NULL);
assert (streq (hello, "Hello"));
assert (streq (world, "World"));
zstr_free (&hello);
zstr_free (&world);

zstr_recvx (capture, &hello, &world, NULL);
assert (streq (hello, "Hello"));
assert (streq (world, "World"));
zstr_free (&hello);
zstr_free (&world);

zsock_destroy (&faucet);
zsock_destroy (&sink);
zsock_destroy (&capture);
zactor_destroy (&proxy);
.fi
.if n \{\
.RE
.\}
.sp
.SH "AUTHORS"
.sp
The czmq manual was written by the authors in the AUTHORS file\&.
.SH "RESOURCES"
.sp
Main web site: \m[blue]\fB\%\fR\m[]
.sp
Report bugs to the email <\m[blue]\fBzeromq\-dev@lists\&.zeromq\&.org\fR\m[]\&\s-2\u[1]\d\s+2>
.SH "COPYRIGHT"
.sp
Copyright (c) 1991\-2012 iMatix Corporation \-\- http://www\&.imatix\&.com Copyright other contributors as noted in the AUTHORS file\&. This file is part of CZMQ, the high\-level C binding for 0MQ: http://czmq\&.zeromq\&.org This Source Code Form is subject to the terms of the Mozilla Public License, v\&. 2\&.0\&. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla\&.org/MPL/2\&.0/\&. LICENSE included with the czmq distribution\&.
.SH "NOTES"
.IP " 1." 4
zeromq-dev@lists.zeromq.org
.RS 4
\%mailto:zeromq-dev@lists.zeromq.org
.RE

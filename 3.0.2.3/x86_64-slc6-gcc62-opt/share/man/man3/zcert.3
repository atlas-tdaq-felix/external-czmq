'\" t
.\"     Title: zcert
.\"    Author: [see the "AUTHORS" section]
.\" Generator: DocBook XSL Stylesheets v1.75.2 <http://docbook.sf.net/>
.\"      Date: 06/18/2018
.\"    Manual: CZMQ Manual
.\"    Source: CZMQ 3.0.2
.\"  Language: English
.\"
.TH "ZCERT" "3" "06/18/2018" "CZMQ 3\&.0\&.2" "CZMQ Manual"
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
zcert \- work with CURVE security certificates
.SH "SYNOPSIS"
.sp
.nf
//  Create and initialize a new certificate in memory
CZMQ_EXPORT zcert_t *
    zcert_new (void);

//  Constructor, accepts public/secret key pair from caller
CZMQ_EXPORT zcert_t *
    zcert_new_from (byte *public_key, byte *secret_key);

//  Destroy a certificate in memory
CZMQ_EXPORT void
    zcert_destroy (zcert_t **self_p);

//  Return public part of key pair as 32\-byte binary string
CZMQ_EXPORT byte *
    zcert_public_key (zcert_t *self);

//  Return secret part of key pair as 32\-byte binary string
CZMQ_EXPORT byte *
    zcert_secret_key (zcert_t *self);

//  Return public part of key pair as Z85 armored string
CZMQ_EXPORT char *
    zcert_public_txt (zcert_t *self);

//  Return secret part of key pair as Z85 armored string
CZMQ_EXPORT char *
    zcert_secret_txt (zcert_t *self);

//  Set certificate metadata from formatted string\&.
CZMQ_EXPORT void
    zcert_set_meta (zcert_t *self, const char *name, const char *format, \&.\&.\&.);

//  Get metadata value from certificate; if the metadata value doesn\'t
//  exist, returns NULL\&.
CZMQ_EXPORT char *
    zcert_meta (zcert_t *self, const char *name);

//  Get list of metadata fields from certificate\&. Caller is responsible for
//  destroying list\&. Caller should not modify the values of list items\&.
CZMQ_EXPORT zlist_t *
    zcert_meta_keys (zcert_t *self);

//  Load certificate from file (constructor)
CZMQ_EXPORT zcert_t *
    zcert_load (const char *filename);

//  Save full certificate (public + secret) to file for persistent storage
//  This creates one public file and one secret file (filename + "_secret")\&.
CZMQ_EXPORT int
    zcert_save (zcert_t *self, const char *filename);

//  Save public certificate only to file for persistent storage
CZMQ_EXPORT int
    zcert_save_public (zcert_t *self, const char *filename);

//  Save secret certificate only to file for persistent storage
CZMQ_EXPORT int
    zcert_save_secret (zcert_t *self, const char *filename);

//  Apply certificate to socket, i\&.e\&. use for CURVE security on socket\&.
//  If certificate was loaded from public file, the secret key will be
//  undefined, and this certificate will not work successfully\&.
CZMQ_EXPORT void
    zcert_apply (zcert_t *self, void *zocket);

//  Return copy of certificate; if certificate is null or we exhausted
//  heap memory, returns null\&.
CZMQ_EXPORT zcert_t *
    zcert_dup (zcert_t *self);

//  Return true if two certificates have the same keys
CZMQ_EXPORT bool
    zcert_eq (zcert_t *self, zcert_t *compare);

//  Print certificate contents to stdout
CZMQ_EXPORT void
    zcert_print (zcert_t *self);

//  DEPRECATED as incompatible with centralized logging
//  Print certificate contents to open stream
CZMQ_EXPORT void
    zcert_fprint (zcert_t *self, FILE *file);

//  Self test of this class
CZMQ_EXPORT void
    zcert_test (bool verbose);
.fi
.SH "DESCRIPTION"
.sp
The zcert class provides a way to create and work with security certificates for the ZMQ CURVE mechanism\&. A certificate contains a public + secret key pair, plus metadata\&. It can be used as a temporary object in memory, or persisted to disk\&. On disk, a certificate is stored as two files\&. One is public and contains only the public key\&. The second is secret and contains both keys\&. The two have the same filename, with the secret file adding "_secret"\&. To exchange certificates, send the public file via some secure route\&. Certificates are not signed but are text files that can be verified by eye\&.
.sp
Certificates are stored in the ZPL (ZMQ RFC 4) format\&. They have two sections, "metadata" and "curve"\&. The first contains a list of \fIname = value\fR pairs, one per line\&. Values may be enclosed in quotes\&. The curve section has a \fIpublic\-key = keyvalue\fR and, for secret certificates, a \fIsecret\-key = keyvalue\fR line\&. The keyvalue is a Z85\-encoded CURVE key\&.
.SH "EXAMPLE"
.PP
\fBExample\ \&1.\ \&From zcert_test method\fR
.sp
.if n \{\
.RS 4
.\}
.nf
//  Create temporary directory for test files
#   define TESTDIR "\&.test_zcert"
zsys_dir_create (TESTDIR);

//  Create a simple certificate with metadata
zcert_t *cert = zcert_new ();
assert (cert);
zcert_set_meta (cert, "email", "ph@imatix\&.com");
zcert_set_meta (cert, "name", "Pieter Hintjens");
zcert_set_meta (cert, "organization", "iMatix Corporation");
zcert_set_meta (cert, "version", "%d", 1);
assert (streq (zcert_meta (cert, "email"), "ph@imatix\&.com"));
zlist_t *keys = zcert_meta_keys (cert);
assert (zlist_size (keys) == 4);
zlist_destroy (&keys);

//  Check the dup and eq methods
zcert_t *shadow = zcert_dup (cert);
assert (zcert_eq (cert, shadow));
zcert_destroy (&shadow);

//  Check we can save and load certificate
zcert_save (cert, TESTDIR "/mycert\&.txt");
assert (zsys_file_exists (TESTDIR "/mycert\&.txt"));
assert (zsys_file_exists (TESTDIR "/mycert\&.txt_secret"));

//  Load certificate, will in fact load secret one
shadow = zcert_load (TESTDIR "/mycert\&.txt");
assert (shadow);
assert (zcert_eq (cert, shadow));
zcert_destroy (&shadow);

//  Delete secret certificate, load public one
int rc = zsys_file_delete (TESTDIR "/mycert\&.txt_secret");
assert (rc == 0);
shadow = zcert_load (TESTDIR "/mycert\&.txt");

//  32\-byte null key encodes as 40 \'0\' characters
assert (streq (zcert_secret_txt (shadow), FORTY_ZEROES));

zcert_destroy (&shadow);
zcert_destroy (&cert);

//  Delete all test files
zdir_t *dir = zdir_new (TESTDIR, NULL);
assert (dir);
zdir_remove (dir, true);
zdir_destroy (&dir);
.fi
.if n \{\
.RE
.\}
.SH "AUTHORS"
.sp
The czmq manual was written by the authors in the AUTHORS file\&.
.SH "RESOURCES"
.sp
Main web site: \m[blue]\fB\%\fR\m[]
.sp
Report bugs to the email <\m[blue]\fBzeromq\-dev@lists\&.zeromq\&.org\fR\m[]\&\s-2\u[1]\d\s+2>
.SH "COPYRIGHT"
.sp
Copyright (c) 1991\-2012 iMatix Corporation \-\- http://www\&.imatix\&.com Copyright other contributors as noted in the AUTHORS file\&. This file is part of CZMQ, the high\-level C binding for 0MQ: http://czmq\&.zeromq\&.org This Source Code Form is subject to the terms of the Mozilla Public License, v\&. 2\&.0\&. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla\&.org/MPL/2\&.0/\&. LICENSE included with the czmq distribution\&.
.SH "NOTES"
.IP " 1." 4
zeromq-dev@lists.zeromq.org
.RS 4
\%mailto:zeromq-dev@lists.zeromq.org
.RE

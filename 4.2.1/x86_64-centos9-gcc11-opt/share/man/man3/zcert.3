'\" t
.\"     Title: zcert
.\"    Author: [see the "AUTHORS" section]
.\" Generator: DocBook XSL Stylesheets vsnapshot <http://docbook.sf.net/>
.\"      Date: 02/21/2023
.\"    Manual: CZMQ Manual
.\"    Source: CZMQ 4.2.1
.\"  Language: English
.\"
.TH "ZCERT" "3" "02/21/2023" "CZMQ 4\&.2\&.1" "CZMQ Manual"
.\" -----------------------------------------------------------------
.\" * Define some portability stuff
.\" -----------------------------------------------------------------
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.\" http://bugs.debian.org/507673
.\" http://lists.gnu.org/archive/html/groff/2009-02/msg00013.html
.\" ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
.ie \n(.g .ds Aq \(aq
.el       .ds Aq '
.\" -----------------------------------------------------------------
.\" * set default formatting
.\" -----------------------------------------------------------------
.\" disable hyphenation
.nh
.\" disable justification (adjust text to left margin only)
.ad l
.\" -----------------------------------------------------------------
.\" * MAIN CONTENT STARTS HERE *
.\" -----------------------------------------------------------------
.SH "NAME"
zcert \- Class for work with CURVE security certificates
.SH "SYNOPSIS"
.sp
.nf
//  This is a stable class, and may not change except for emergencies\&. It
//  is provided in stable builds\&.
//  This class has draft methods, which may change over time\&. They are not
//  in stable releases, by default\&. Use \-\-enable\-drafts to enable\&.
//  Create and initialize a new certificate in memory
CZMQ_EXPORT zcert_t *
    zcert_new (void);

//  Accepts public/secret key pair from caller
CZMQ_EXPORT zcert_t *
    zcert_new_from (const byte *public_key, const byte *secret_key);

//  Load certificate from file
CZMQ_EXPORT zcert_t *
    zcert_load (const char *filename);

//  Destroy a certificate in memory
CZMQ_EXPORT void
    zcert_destroy (zcert_t **self_p);

//  Return public part of key pair as 32\-byte binary string
CZMQ_EXPORT const byte *
    zcert_public_key (zcert_t *self);

//  Return secret part of key pair as 32\-byte binary string
CZMQ_EXPORT const byte *
    zcert_secret_key (zcert_t *self);

//  Return public part of key pair as Z85 armored string
CZMQ_EXPORT const char *
    zcert_public_txt (zcert_t *self);

//  Return secret part of key pair as Z85 armored string
CZMQ_EXPORT const char *
    zcert_secret_txt (zcert_t *self);

//  Set certificate metadata from formatted string\&.
CZMQ_EXPORT void
    zcert_set_meta (zcert_t *self, const char *name, const char *format, \&.\&.\&.) CHECK_PRINTF (3);

//  Get metadata value from certificate; if the metadata value doesn\*(Aqt
//  exist, returns NULL\&.
CZMQ_EXPORT const char *
    zcert_meta (zcert_t *self, const char *name);

//  Get list of metadata fields from certificate\&. Caller is responsible for
//  destroying list\&. Caller should not modify the values of list items\&.
CZMQ_EXPORT zlist_t *
    zcert_meta_keys (zcert_t *self);

//  Save full certificate (public + secret) to file for persistent storage
//  This creates one public file and one secret file (filename + "_secret")\&.
CZMQ_EXPORT int
    zcert_save (zcert_t *self, const char *filename);

//  Save public certificate only to file for persistent storage
CZMQ_EXPORT int
    zcert_save_public (zcert_t *self, const char *filename);

//  Save secret certificate only to file for persistent storage
CZMQ_EXPORT int
    zcert_save_secret (zcert_t *self, const char *filename);

//  Apply certificate to socket, i\&.e\&. use for CURVE security on socket\&.
//  If certificate was loaded from public file, the secret key will be
//  undefined, and this certificate will not work successfully\&.
CZMQ_EXPORT void
    zcert_apply (zcert_t *self, void *socket);

//  Return copy of certificate; if certificate is NULL or we exhausted
//  heap memory, returns NULL\&.
//  Caller owns return value and must destroy it when done\&.
CZMQ_EXPORT zcert_t *
    zcert_dup (zcert_t *self);

//  Return true if two certificates have the same keys
CZMQ_EXPORT bool
    zcert_eq (zcert_t *self, zcert_t *compare);

//  Print certificate contents to stdout
CZMQ_EXPORT void
    zcert_print (zcert_t *self);

//  Self test of this class
CZMQ_EXPORT void
    zcert_test (bool verbose);

#ifdef CZMQ_BUILD_DRAFT_API
//  *** Draft method, for development use, may change without warning ***
//  Accepts public/secret key text pair from caller
CZMQ_EXPORT zcert_t *
    zcert_new_from_txt (const char *public_txt, const char *secret_txt);

//  *** Draft method, for development use, may change without warning ***
//  Unset certificate metadata\&.
CZMQ_EXPORT void
    zcert_unset_meta (zcert_t *self, const char *name);

#endif // CZMQ_BUILD_DRAFT_API
Please add \*(Aq@interface\*(Aq section in \*(Aq\&./\&.\&./src/zcert\&.c\*(Aq\&.
.fi
.SH "DESCRIPTION"
.sp
The zcert class provides a way to create and work with security certificates for the ZMQ CURVE mechanism\&. A certificate contains a public + secret key pair, plus metadata\&. It can be used as a temporary object in memory, or persisted to disk\&. On disk, a certificate is stored as two files\&. One is public and contains only the public key\&. The second is secret and contains both keys\&. The two have the same filename, with the secret file adding "_secret"\&. To exchange certificates, send the public file via some secure route\&. Certificates are not signed but are text files that can be verified by eye\&.
.sp
Certificates are stored in the ZPL (ZMQ RFC 4) format\&. They have two sections, "metadata" and "curve"\&. The first contains a list of \fIname = value\fR pairs, one per line\&. Values may be enclosed in quotes\&. The curve section has a \fIpublic\-key = keyvalue\fR and, for secret certificates, a \fIsecret\-key = keyvalue\fR line\&. The keyvalue is a Z85\-encoded CURVE key\&.
.SH "EXAMPLE"
.PP
\fBFrom zcert_test method\fR. 
.sp
.if n \{\
.RS 4
.\}
.nf
const char *SELFTEST_DIR_RW = "src/selftest\-rw";

const char *testbasedir  = "\&.test_zcert";
const char *testfile = "mycert\&.txt";
char *basedirpath = NULL;   // subdir in a test, under SELFTEST_DIR_RW
char *filepath = NULL;      // pathname to testfile in a test, in dirpath
char *filepath_s = NULL;    // pathname to testfile+secret in a test, in dirpath

basedirpath = zsys_sprintf ("%s/%s", SELFTEST_DIR_RW, testbasedir);
assert (basedirpath);
filepath = zsys_sprintf ("%s/%s", basedirpath, testfile);
assert (filepath);
filepath_s = zsys_sprintf ("%s_secret", filepath);
assert (filepath_s);

// Make sure old aborted tests do not hinder us
zdir_t *dir = zdir_new (basedirpath, NULL);
if (dir) {
    zdir_remove (dir, true);
    zdir_destroy (&dir);
}
zsys_file_delete (filepath);
zsys_dir_delete  (basedirpath);

//  Create temporary directory for test files
zsys_dir_create (basedirpath);

//  Create a simple certificate with metadata
zcert_t *cert = zcert_new ();
assert (cert);
zcert_set_meta (cert, "email", "ph@imatix\&.com");
zcert_set_meta (cert, "name", "Pieter Hintjens");
zcert_set_meta (cert, "organization", "iMatix Corporation");
zcert_set_meta (cert, "version", "%d", 1);
zcert_set_meta (cert, "delete_me", "now");
zcert_unset_meta (cert, "delete_me");
assert (streq (zcert_meta (cert, "email"), "ph@imatix\&.com"));
zlist_t *keys = zcert_meta_keys (cert);
assert (zlist_size (keys) == 4);
zlist_destroy (&keys);

//  Check the dup and eq methods
zcert_t *shadow = zcert_dup (cert);
assert (zcert_eq (cert, shadow));
zcert_destroy (&shadow);

//  Check we can save and load certificate
zcert_save (cert, filepath);
assert (zsys_file_exists (filepath));
assert (zsys_file_exists (filepath_s));

//  Load certificate, will in fact load secret one
shadow = zcert_load (filepath);
assert (shadow);
assert (zcert_eq (cert, shadow));
zcert_destroy (&shadow);

//  Delete secret certificate, load public one
int rc = zsys_file_delete (filepath_s);
assert (rc == 0);
shadow = zcert_load (filepath);

//  32\-byte null key encodes as 40 \*(Aq0\*(Aq characters
assert (streq (zcert_secret_txt (shadow), FORTY_ZEROES));

#ifdef CZMQ_BUILD_DRAFT_API
// test zcert_from_txt
zcert_t *cert2 = zcert_new_from_txt(cert\->public_txt, cert\->secret_txt);
assert (cert2);
assert (zcert_eq (cert, cert2));
zcert_destroy(&cert2);
#endif

zcert_destroy (&shadow);
zcert_destroy (&cert);

//  Delete all test files
dir = zdir_new (basedirpath, NULL);
assert (dir);
zdir_remove (dir, true);
zdir_destroy (&dir);

zstr_free (&basedirpath);
zstr_free (&filepath);
zstr_free (&filepath_s);

#if defined (__WINDOWS__)
zsys_shutdown();
#endif
.fi
.if n \{\
.RE
.\}
.sp
.SH "AUTHORS"
.sp
The czmq manual was written by the authors in the AUTHORS file\&.
.SH "RESOURCES"
.sp
Main web site: \m[blue]\fB\%\fR\m[]
.sp
Report bugs to the email <\m[blue]\fBzeromq\-dev@lists\&.zeromq\&.org\fR\m[]\&\s-2\u[1]\d\s+2>
.SH "COPYRIGHT"
.sp
Copyright (c) the Contributors as noted in the AUTHORS file\&. This file is part of CZMQ, the high\-level C binding for 0MQ: http://czmq\&.zeromq\&.org\&. This Source Code Form is subject to the terms of the Mozilla Public License, v\&. 2\&.0\&. If a copy of the MPL was not distributed with this file, You can obtain one at http://mozilla\&.org/MPL/2\&.0/\&. LICENSE included with the czmq distribution\&.
.SH "NOTES"
.IP " 1." 4
zeromq-dev@lists.zeromq.org
.RS 4
\%mailto:zeromq-dev@lists.zeromq.org
.RE
